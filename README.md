## Template

#### Link

- https://pkg.go.dev/text/template
- https://colobu.com/2019/11/05/Golang-Templates-Cheatsheet/ **(比较重要)**
- https://levelup.gitconnected.com/learn-and-use-templates-in-go-aa6146b01a38

#### 解析模板

```shell script
template.ParseFiles
```

#### 解析字符串模板

```shell script
t, err := template.New("fool").Parse("Name: {{.Name}}, Age: {{.Age}}")
```

输出:

`Name: SHUMIN, Age: 30`

#### 如果为空就painc
```shell script
var t = template.Must(template.New("name").Parse("html"))
```


#### 执行模板

```shell script
tpl.Execute(io.Writer, data)
```

#### 执行命名的模板

```shell script
tpl.ExecuteTemplate(io.Writer, name, data)
```