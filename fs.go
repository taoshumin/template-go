/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package template

import (
	"io/fs"
	"path/filepath"
)

// ToIsRegular 确保文件是常规文件并且可以打开
func ToIsRegular(ffs fs.FileInfo) bool {
	return ffs.Mode().IsRegular()
}

// ToSlash 将平台中相关的分割符号(window-> \, mac and liunx /)
func ToSlash(path string) string  {
	return filepath.ToSlash(path)
}

// ToBase 获取最后一个分割符之后的部分（不包括分割符）
// for example: /data/syncthing/kmci3-tuqhj.txt
// want: kmci3-tuqhj.txt
func ToBase(path string) string  {
	return filepath.Base(path)
}

// ToDir 获取文件夹路径
// for example: a/b/c/d
// want: a/b/c
func ToDir(path string) string  {
	return filepath.Dir(path)
}

// ToExt 获取文件的扩展名
// for example: hello.txt -> txt
// shshsks.csv ->cvs
func ToExt(path string) string  {
	return filepath.Ext(path)
}

// ToRel 相对路径
func ToRel(basePath,path string) (string,error) {
	return filepath.Rel(basePath,path)
}

// ToClean 清除path中多余的字符
func ToClean(path string) string  {
	return filepath.Clean(path)
}

// ToIsAbs 判断该路径是否是绝对路径
func ToIsAbs(path string) bool  {
	return filepath.IsAbs(path)
}

// ToAbs 获取path的绝对路径
func ToAbs(path string) (string,error) {
	return filepath.Abs(path)
}

// ToVolumeName 获取挂在卷的名称
// for example F:\\a\b -> F
func ToVolumeName(path string) string  {
	return filepath.VolumeName(path)
}

// ToGlob 列出当前文件夹符合的路径
func ToGlob(match string) ([]string,error)  {
	return filepath.Glob(match)
}

// ToWalk 遍历指定目录(包括子目录)，对遍历的项目用walkFn函数进行处理.
func ToWalk(path string) error  {
	return filepath.Walk(path, func(path string, info fs.FileInfo, err error) error {
		return nil
	})
}