/*
Copyright 2021 The Gridsum Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package template

import "testing"

func TestToSlash(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "widnow",
			args: args{
				path:  "D:\\COMPasS\\folder\\folderA",
			},
			want:  "D:\\COMPasS\folder\folderA",
		},
		{
			name: "liunx",
			args: args{
				path: "/data/syncthing/kmci3-tuqhj",
			},
			want: "/data/syncthing/kmci3-tuqhj",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := ToSlash(tt.args.path);
			if got != tt.want {
				t.Errorf("ToSlash() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestToBase(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "widnow",
			args: args{
				path:  "D:\\COMPasS\\folder\\folderA\\hellow.txt",
			},
			want:  "hellow.txt",
		},
		{
			name: "liunx",
			args: args{
				path: "/data/syncthing/kmci3-tuqhj/1.xyhwh.txt",
			},
			want: "1.xyhwh.txt",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := ToBase(tt.args.path);
			if got != tt.want {
				t.Errorf("ToSlash() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestToDir(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "widnow",
			args: args{
				path:  "D:\\COMPasS\\folder\\folderA\\hellow.txt",
			},
			want:  "D:\\COMPasS\folder\folderA",
		},
		{
			name: "liunx",
			args: args{
				path: "/data/syncthing/kmci3-tuqhj/1.xyhwh.txt",
			},
			want: "/data/syncthing/kmci3-tuqhj",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := ToDir(tt.args.path);
			if got != tt.want {
				t.Errorf("ToSlash() = %v, want %v", got, tt.want)
			}
		})
	}
}