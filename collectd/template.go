/*
Copyright 2021 The Xiadat Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package collectd

import (
	"bytes"
	"html/template"
)

func TemplateNew() ([]byte, error) {
	tpl, err := template.New("foo").Parse("Name: {{.Name}}, Age: {{.Age}}")
	if err != nil {
		return nil, err
	}

	type User struct {
		Name string
		Age  int
	}

	usr := User{
		Name: "SHUMIN",
		Age:  30,
	}
	bt := bytes.Buffer{}
	err = tpl.Execute(&bt, usr)
	if err != nil {
		return nil, err
	}
	return bt.Bytes(), nil
}

func TemplateParseFiles(file string) ([]byte, error) {
	tpl, err := template.ParseFiles(file)
	if err != nil {
		return nil, err
	}
	type User struct {
		Name   string
		Coupon string
		Amount int64
	}
	usr := User{
		Name:   "Rick",
		Coupon: "IAMAWESOMEGOPHER",
		Amount: 5000,
	}
	bt := bytes.Buffer{}
	err = tpl.Execute(&bt, usr)
	if err != nil {
		return nil, err
	}
	return bt.Bytes(), err
}

func TemplateSlice(file string) ([]byte, error) {
	tpl, err := template.ParseFiles(file)
	if err != nil {
		return nil, err
	}
	sages := []string{"Tom Cruise", "Jack Nicholson", "Demi Moore", "Kevin Bacon", "Wolfgang Bodison"}
	bt := bytes.Buffer{}
	err = tpl.Execute(&bt, sages)
	if err != nil {
		return nil, err
	}
	return bt.Bytes(), err
}

func TemplateMap(file string) ([]byte, error) {
	tpl, err := template.ParseFiles(file)
	if err != nil {
		return nil, err
	}

	superheroes := map[string]string{
		"Lt. Daniel Kaffee":           "Tom Cruise",
		"Col. Nathan R. Jessep":       "Jack Nicholson",
		"Lt. Cdr. JoAnne Galloway":    "Demi Moore",
		"Capt. Jack Ross":             "Kevin Bacon",
		"Lance Cpl. Harold W. Dawson": "Wolfgang Bodison",
	}

	bt := bytes.Buffer{}
	err = tpl.Execute(&bt, superheroes)
	if err != nil {
		return nil, err
	}
	return bt.Bytes(), err
}

func TemplateStruct(file string) ([]byte, error) {
	tpl, err := template.ParseFiles(file)
	if err != nil {
		return nil, err
	}
	superhero := struct {
		Name  string
		Motto string
	}{
		Name:  "Bruise Wayne",
		Motto: "I am batman",
	}

	bt := bytes.Buffer{}
	err = tpl.Execute(&bt, superhero)
	if err != nil {
		return nil, err
	}
	return bt.Bytes(), err
}

func TemplateSliceStruct(file string) ([]byte, error) {
	tpl, err := template.ParseFiles(file)
	if err != nil {
		return nil, err
	}

	type superhero struct {
		Name  string
		Motto string
	}
	im := superhero{
		Name:  "Iron man",
		Motto: "I am iron man",
	}
	ca := superhero{
		Name:  "Captain America",
		Motto: "Avengers assemble",
	}
	ds := superhero{
		Name:  "Doctor Strange",
		Motto: "I see things",
	}
	superheroes := []superhero{im, ca, ds}

	bt := bytes.Buffer{}
	err = tpl.Execute(&bt, superheroes)
	if err != nil {
		return nil, err
	}
	return bt.Bytes(), err
}
