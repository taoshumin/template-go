/*
Copyright 2021 The Xiadat Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package collectd

import (
	"reflect"
	"testing"
)

func TestTemplateParseFiles(t *testing.T) {
	type args struct {
		file string
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name: "parese",
			args: args{
				file: "index.tmpl",
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := TemplateParseFiles(tt.args.file)
			if (err != nil) != tt.wantErr {
				t.Errorf("TemplateParseFiles() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TemplateParseFiles() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTemplateSlice(t *testing.T) {
	type args struct {
		file string
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name:    "slice",
			args:    args{file: "slice.tmpl"},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := TemplateSlice(tt.args.file)
			if (err != nil) != tt.wantErr {
				t.Errorf("TemplateSlice() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TemplateSlice() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTemplateMap(t *testing.T) {
	type args struct {
		file string
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name:    "map",
			args:    args{file: "map.tmpl"},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := TemplateMap(tt.args.file)
			if (err != nil) != tt.wantErr {
				t.Errorf("TemplateMap() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TemplateMap() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTemplateStruct(t *testing.T) {
	type args struct {
		file string
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name:    "struct",
			args:    args{file: "struct.tmpl"},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := TemplateStruct(tt.args.file)
			if (err != nil) != tt.wantErr {
				t.Errorf("TemplateStruct() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TemplateStruct() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTemplateSliceStruct(t *testing.T) {
	type args struct {
		file string
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name:    "slice_struct",
			args:    args{file: "slice.struct.tmpl"},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := TemplateSliceStruct(tt.args.file)
			if (err != nil) != tt.wantErr {
				t.Errorf("TemplateSliceStruct() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TemplateSliceStruct() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTemplateNew(t *testing.T) {
	tests := []struct {
		name    string
		want    []byte
		wantErr bool
	}{
		{
			name:    "new",
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := TemplateNew()
			if (err != nil) != tt.wantErr {
				t.Errorf("TemplateNew() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TemplateNew() got = %v, want %v", got, tt.want)
			}
		})
	}
}
